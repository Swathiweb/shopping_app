
# frozen_string_literal: true

require 'spec_helper'

describe API::ProductsController do
  describe "GET index" do
    it "get products" do
      product = Product.create(name: "Product1")
      get :index
      expect(response).to have_http_status(:ok)
    end

    it "renders the show response" do
      product = Product.create(name: "Product1")
      get :show, params: {id: product.id}
      expect(response).to have_http_status(:ok)
    end
  end
end