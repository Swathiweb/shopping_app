Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    resources :products do 
      collection do
        post :add_to_cart
        delete :remove_from_cart
        post :place_an_order
        get :cart_items
        get :orders
      end
    end
  end
end
