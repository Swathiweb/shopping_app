class CartItem < ApplicationRecord
  validates_presence_of  :product_id
  validates_presence_of  :price
  validates_presence_of  :user_id

end
