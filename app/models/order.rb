class Order < ApplicationRecord
  validates_presence_of  :product_id
  validates_presence_of  :price
end
