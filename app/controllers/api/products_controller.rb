class Api::ProductsController < ActionController::Base
  skip_before_action :verify_authenticity_token

  def index
    @products = Product.all
    if @products.present?
      @products = @products.where("name like ?", "%#{params[:search]}%") if params[:search].present?
      @products = @products.paginate(:page => params[:page], :per_page => 1)
      render json: @products, status: :ok
    else
      render json: {message: "Products Not Available"}, status: :not_found
    end
  end

  def show
    @product = Product.where(id: params[:id]).first
    if @product.present?
      render json: @product, status: :ok
    else
      render json: {message: "Products Not Found"}, status: :not_found
    end
  end

  def add_to_cart
    @cart_item = CartItem.create(product_id: params[:product_id], price: params[:price], user_id: params[:user_id])
    if @cart_item.save
      render json: @cart_item, status: :ok
    else
      render json: {errors: @cart_item.errors.full_messages}, status: :not_found
    end
  end

  def remove_from_cart
    @cart_item = CartItem.find(params[:id])
    if @cart_item.present?
      @cart_item.destroy
      render json: {message: "Successfully removed from cart"}, status: :ok
    else
      render json: {message: "CartItem Not Found"}, status: :not_found
    end
  end

  def cart_items
    @cart_items = CartItem.where(user_id: params[:user_id])
    @total = 0
    @cart_items.each do |item|
      @total = @total + item.price
    end
    if @cart_items.present?
      render json: @cart_items, status: :ok
    else
      render json: { message: "CartItems Not Found"}, status: :not_found
    end
  end


  def place_an_order
    @order = Order.create(product_id: params[:product_id], price: params[:price], quantity: params[:quantity], transaction_id: params[:transaction_id])
    if @order.save
      OrderMailer.with(order: @order, email: params[:user_id]).new_order_email.deliver_later
      render json: @order, status: :ok
    else
      render json: {message: "Order not Placed"}, status: :not_found
    end
  end

  def orders
    @orders = Order.all
    @total = 0
    @orders = @orders.where(user_id: params[:user_id]) if params[:user_id].present?
    @orders = @orders.where(:created_at.gte => params[:date_time]) if params[:date_time].present?
    if @orders.present?
      @orders.each do |item|
        @total = @total + item.price
      end
      render json: @orders, status: :ok
    else
      render json: {message: "Orders Not Found"}, status: :not_found
    end
  end

end
