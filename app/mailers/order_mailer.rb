class OrderMailer < ApplicationMailer
  def new_order_email(product, email)
    @order = params[:order]
    mail(to: params[:email], subject: "You Placed an new order!")
  end
end
