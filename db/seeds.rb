# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

ActiveRecord::Base.connection.execute('products');
products_list = [
    [ 'Product1', 'For best look', 1 ],
    [ 'New Brand', 'For better health', 3 ],
    [ 'Product55', 'For Moderate people', 4 ],
    [ 'Brand22', 'For new appearance', 5 ]
]

products_list.each do |name, description, quantity|
  Product.create(name: name, description: description, quantity: quantity)
end
