class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.integer :product_id
      t.integer :price
      t.integer :quantity
      t.string :transaction_id

      t.timestamps
    end
  end
end
