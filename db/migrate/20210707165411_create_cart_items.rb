class CreateCartItems < ActiveRecord::Migration[6.0]
  def change
    create_table :cart_items do |t|
      t.integer :product_id
      t.string :user_id
      t.integer :price

      t.timestamps
    end
  end
end
